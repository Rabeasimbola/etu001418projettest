package model;
import annotation.Annotation;
import modelv.ModelV;
public class Employer {
    int id;
    String nom;
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {
        this.nom = nom;
    }
    @Annotation(geturl="listerEmployer")
    public ModelV lister() throws Exception{
        ModelV mv = new ModelV();
        String[] liste = {"Boaykely","Joston"};
        mv.setObjt(liste);
        mv.setPage("lister.jsp");
        return mv;
    }
     @Annotation(geturl="saveEmployer")
    public ModelV save() throws Exception{
        Connection c = null;
//        this.create(this.getConnexion());
        ModelV mv = new ModelV();
        String message = "reussi";
        mv.setObjt(message);
        mv.setPage("index.html");
        return mv;
    }
}
