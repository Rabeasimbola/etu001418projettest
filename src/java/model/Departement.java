/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import annotation.Annotation;
import modelv.ModelV;

/**
 *
 * @author Rabeasimbola
 */
public class Departement {
    int id;
    String nom;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    @Annotation(geturl="lister")
    public ModelV lister() throws Exception{
        ModelV mv = new ModelV();
        String[] liste = {"Kasy","Zanjy"};
        mv.setObjt(liste);
        mv.setPage("lister.jsp");
        return mv;
    }
     @Annotation(geturl="save")
    public ModelV save() throws Exception{
        Connection c = null;
//        this.create(this.getConnexion());
        ModelV mv = new ModelV();
        String message = "reussi";
        mv.setObjt(message);
        mv.setPage("index.html");
        return mv;
    }
}
